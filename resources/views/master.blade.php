<!doctype html>
<html lang="en">
<head>
    @include('layouts.begin')
</head>

<body class="bg-light">

    <div id="app">
        <div class="container">
            <div class="py-5 text-center">
                <img class="d-block mx-auto mb-4" src="{{asset('files/Bugloos-Logo.svg')}}" alt="Bugloos" height="50">
                <h2>Milad Ghofrani - Bugloos Project - @yield('title')</h2>
            </div>

            @yield('content')

            <footer class="my-5 pt-5 text-muted text-center text-small">
                <p class="mb-1">&copy; 2021 Bugloos</p>
            </footer>
        </div>
    </div>

    @include('layouts.end')

</body>

</html>
