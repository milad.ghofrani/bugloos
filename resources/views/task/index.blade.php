@extends('master')

@section('title','Task')

@section('content')
    <div class="row">
        <div class="col-md-12 font_size_8">

            <bugloos-table route="{{route('task.bugloosTable')}}"></bugloos-table>

        </div>
    </div>
@endsection
