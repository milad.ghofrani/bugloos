import Vue from 'vue';
import axios from 'axios';
import bugloosTable from './components/bugloos-table.vue'
import VueSweetalert2 from 'vue-sweetalert2';
import '@fortawesome/fontawesome-free/js/all.js';
import 'bootstrap';

Vue.use(VueSweetalert2);

window.Vue = Vue;
window.axios = axios;
window.VueSweetalert2 = VueSweetalert2;

const app = new Vue({
    components: {
        bugloosTable,
    },
}).$mount('#app');
