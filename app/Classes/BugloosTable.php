<?php

namespace App\Classes;

class BugloosTable {

    protected static $data;
    protected static $columns;
    protected static $filter;
    public static $rows;
    public static $total;

    public static function loadTable($data,$columns,$filter,$defaultSort,$defaultSortType,$json = false){

        self::$data = $data;
        self::$columns = $columns;
        self::$filter = $filter;

        // If the user has not yet selected a column for sorting, the default values for sorting will be selected
        if(!self::$filter['sort']){
            self::$filter['sort'] = $defaultSort;
            self::$filter['sort_type'] = $defaultSortType;
        }

        // If a user enters a text search
        if(self::$filter['search'] && !empty(self::$filter['search'])){
            if($json){
                // If we get data from json file
                self::jsonSearch(self::$filter['search']);
            } else {
                // If we get data from model
                self::modelSearch(self::$filter['search']);
            }
        }

        // Total count of data
        self::$total = self::$data->count();

        // Sort and Paginate collection
        if($json){
            self::jsonCollection();
        } else {
            self::modelCollection();
        }

        return [self::$rows, self::$total];

    }

    public static function jsonCollection(){
        if(self::$filter['sort_type'] == 'asc'){
            self::$rows = self::$data->skip(self::$filter['skip'])->take(self::$filter['entries'])->sortBy(self::$filter['sort'])->values()->all();
        } else {
            self::$rows = self::$data->skip(self::$filter['skip'])->take(self::$filter['entries'])->sortByDesc(self::$filter['sort'])->values()->all();
        }
    }

    public static function modelCollection(){
        self::$rows = self::$data->skip(self::$filter['skip'])->take(self::$filter['entries'])->orderBy(self::$filter['sort'],self::$filter['sort_type'])->get();
    }

    public static function jsonSearch($text){
        // Make search text trim and lowercase
        $searchText = self::stringForJsonSearch($text);

        $mergeCollection = [];
        // Check all columns for search
        foreach (self::$columns as $key => $column) {
            // We check each column one by one to check the search permission
            if(isset($column['searchable']) && $column['searchable'] == true) {
                // Search in each column with search permission
                $collect_{$key} = self::$data->filter(function ($data) use ($searchText, $column) {
                    if (isset($column['sort_field'])) {
                        // if field name changed by user
                        return strstr(strtolower($data->{$column['sort_field']}), $searchText);
                    } else {
                        return strstr(strtolower($data->{$column['field']}), $searchText);
                    }
                });
                // Merge all find rows
                $mergeCollection = array_merge($mergeCollection, $collect_{$key}->toArray());
            }
        }
        // Delete duplicate rows
        $mergeCollection = array_map("unserialize", array_unique(array_map("serialize", $mergeCollection)));
        // Save rows collection
        self::$data = collect($mergeCollection);
    }

    public static function modelSearch($text){
        // Make search text trim and available for database search
        $searchText = self::anyStrInSearch($text);

        // Check all columns for search
        foreach (self::$columns as $column){
            // We check each column one by one to check the search permission
            if(isset($column['searchable']) && $column['searchable'] == true){
                // Search in each column with search permission
                if(isset($column['sort_field'])){
                    // if field name changed by user
                    self::$data->orWhere($column['sort_field'], 'like', $searchText);
                } else{
                    self::$data->orWhere($column['field'], 'like', $searchText);
                }
            }
        }
    }

    public static function anyStrInSearch($input){
        $input = trim($input);
        $input = str_replace(' ', '%', $input);
        $input = '%' . $input . '%';
        return $input;
    }

    public static function stringForJsonSearch($input){
        $input = trim($input);
        $input = strtolower($input);
        return $input;
    }
}
