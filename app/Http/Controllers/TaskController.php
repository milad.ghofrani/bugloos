<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Classes\BugloosTable;

class TaskController extends Controller
{
    public function index(Request $request){

//        Task::factory()->count(100)->create();

        return view('task.index');
    }

    public function bugloosTable(Request $request){

        // Get table filter -> sort, sort_type, entries, skip, search
        $filter = $request->get('filter');
        // Default sort column
        $defaultSort = 'title';
        // Default sort type -> asc | desc
        $defaultSortType = 'asc';

        /*
         * title [string]
         * field [string]
         * sort_field (optional -> when field name is not exist in database) [string]
         * width (optional) [int]
         * sortable (optional) [true,false]
         * searchable (optional) [true,false]
         * */

        $columns = [
            ['title' => 'Company name', 'field' => 'title', 'sortable' => true, 'searchable' => true],
            ['title' => 'SAP number', 'field' => 'sap', 'width' => '15', 'sortable' => true, 'searchable' => true],
            ['title' => 'city and Country', 'field' => 'cityCountry', 'sort_field' => 'city', 'sortable' => true, 'searchable' => true],
            ['title' => 'Sub/dealer', 'field' => 'SubDealer', 'sort_field' => 'sub', 'sortable' => true, 'searchable' => true],
            ['title' => 'Machines', 'field' => 'machines', 'width' => '10', 'sortable' => true],
            ['title' => 'Alarms/warning', 'field' => 'active', 'width' => '5'],
            ['title' => 'Status', 'field' => 'status', 'width' => '10', 'sortable' => true],
        ];

        // you can use data from databse
        $data = Task::query();

        // you can use data from api with json format
        $jsonFile = file_get_contents(public_path('files/tasks.json'));
        $jsonFile = json_decode($jsonFile);
//        $data = collect($jsonFile);

        // Run loadTable from BugloosTable class to generate table
        BugloosTable::loadTable($data,$columns,$filter,$defaultSort,$defaultSortType);
        $rows = BugloosTable::$rows;
        $total = BugloosTable::$total;

        // You can customize some columns
        foreach ($rows as $row){
            $row->cityCountry = $row->city . ", " . $row->country;
            $row->SubDealer = $row->sub . ", " . $row->dealer;
            $row->status = ($row->status == 1) ? '<span class="green_circle"></span> Active' : '<span class="red_circle"></span> Inactive' ;
        }

        return [
            'rows' => $rows,
            'total' => $total,
            'columns' => $columns,
            'sort' => $defaultSort,
            'sort_type' => $defaultSortType,
        ];
    }
}
