<?php

namespace Database\Factories;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->company,
            'sap' => rand(1111111,9999999),
            'city' => $this->faker->city,
            'country' => $this->faker->country,
            'sub' => $this->faker->company,
            'dealer' => $this->faker->name,
            'machines' => rand(1,100),
            'active' => rand(0,20),
            'status' => rand(0,1),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
